# encoding: utf-8
# copyright: 2017, The Authors

title 'Check Azure'

# you add controls here
control 'azure-vm' do                        # A unique ID for this control
  impact 0.7                                # The criticality, if this control fails.
  title 'Checks that the machine was built from the correct image'             # A human-readable title
  describe azure_virtual_machine(name: 'prem-dc', group_name: 'premera-dc') do
    its('sku') { should eq '2012-R2-Datacenter' }
    its('publisher') { should eq 'MicrosoftWindowsServer' }
    its('offer') { should eq 'WindowsServer' }
  end
end

control 'azure_data_disk' do
  impact 1.0
  title 'Checks there is only one data disk and that caching is disabled (AD DC requirement)'
  describe azure_virtual_machine_data_disk(name: 'prem-dc', group_name: 'premera-dc') do
    it { should have_data_disks }
    its('count') { should eq 1 }
    its('caching') { should cmp 'none' }
  end
end

control 'azure_networking' do
  impact 1.0
  title 'Checks there is only one NIC and it is connected to the correct address space'
  describe azure_generic_resource(group_name: 'premera-dc', name: 'prem-dc') do
    its('properties.networkProfile.networkInterfaces.count') { should eq 1 }
  end
  describe azure_generic_resource(group_name: 'premera-dc', name: 'dc-vnet') do
    its('properties.subnets.count') { should eq 1}
    its('properties.subnets.first.name') { should cmp 'dc-subnet' }
    its('properties.addressSpace.addressPrefixes') { should include '10.1.1.0/24' }
  end 
end