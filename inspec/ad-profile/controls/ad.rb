title 'Prep AD Server'

# you add controls here
control '7zip and conemu' do # A unique ID for this control
  impact 0.5 # The criticality, if this control fails.
  title 'Checks that 7zip and conemu have been installed by chocolatey' # A human-readable title
  describe chocolatey_package('7zip') do
    it { should be_installed }
    its('version') { should cmp >= '18.5' }
  end
  describe chocolatey_package('ConEmu') do
    it { should be_installed }
    its('version') { should cmp >= '18.5' }
  end
end

control 'powershell-version' do
  impact 1.0
  title 'Verify Powershell Version'
  # http://inspec.io/docs/reference/resources/powershell/
  script = <<-EOH
    $PSVersionTable.PSVersion -split ' '[0]
  EOH

  describe powershell(script) do
    its('stdout') { should include '5.1' }
  end
end

control 'active-directory' do
  impact 1.0
  title 'Verify Active Directory'
  describe directory('F:\NTDS') do
    it { should exist }
  end
  describe directory('F:\SYSVOL') do
    it { should exist }
  end
  # http://inspec.io/docs/reference/resources/windows_feature/
  describe windows_feature('Active Directory Domain Services') do
    it { should be_installed }
  end
end