# Terraform

This is for demo purposes only.  Do not use a single AD node in production.  During provisioning Powershell Remoting is enabled to use CredSSP authentication and Powershell is upgraded to 5.1.  The machine is prepped then for further work by Inspec and Ansible.

<<toc>>

## Deploy Terraform State Resources

Modify `config/global.tfvars` as needed.

``` bash
cd tfstate-deploy-pipeline/terraform/azure
terraform init
terraform plan -var-file=../../../config/global.tfvars
terraform apply -var-file=../../../config/global.tfvars
```

## Deploy Core Resources

``` bash
cd core-deploy-pipeline/terraform/azure
tf workspace new WORKSPACE_NAME
terraform init
terraform plan -var-file=../../../config/global.tfvars
terraform apply -var-file=../../../config/global.tfvars
```

## Deploy a Windows 2012 R2 node as a single AD DC

`cd windows2012r2-deploy-pipeline/terraform/azure`

1. Create and customize a `custom.tfvars` file based on the `.example` file.

``` bash
tf workspace select WORKSPACE_NAME
terraform init
tf plan -var-file=../../../config/global.tfvars  -var-file=custom.tfvars -out=prem.tfplan
tf apply prem.tfplan
```

As the plan is applying:

**Terraform** is `Infrastructure as Code`
Infrastructure is described using a high-level configuration syntax/domain-specific-language (DSL).  This allows a blueprint of your infrastructure to be versioned and treated like any other code, including storing in version control.  The Terraform code is relatively easy to learn.

**Terraform** has the concept of `Execution Plans`
Terraform `plan` generates  the `execution plan`.  The execution plan shows what Terraform will do when you call `apply`.

**Terraform** builds a `Resource Graph`
It parallelizes the creation and modification of non-dependent resources.  Terraform builds infrastructure as efficiently as possible and operations staff get insight into the dependencies in their infrastructure.

**Terraform** is `Change Automation`
Complex changesets can be applied to your infrastructure with minimal human interaction.  The `execution plan` shows you exactly what Terraform will change and in what order, avoiding many possible human errors.

## Test/Verify with Inspec

**Inspec** is `Continous Compliance` or `Compliance as Code`

* `Easy to Understand`: Requirements are written into versioned, executable, human-readable code.
* `Cross-Functional`: Controls can be written in collaboration between security, developers, and operations.
* `Detect Fleet-wide Issues`: Agentless detect mode can help quickly assess, across the entire fleet, your exposure level.
* `Satisfy Audits`:  Provide compliance reports to auditors at any point in time.
* `Reduce Ambiguity`: Documents leave configuration and processes open to interpretation.  Executable code removes conversations about what should be assessed and turns them into tangible tests with clear intent.
* `Keep up with Threats`: You can write and publish detection code the same day a vulnerability is released and write new rules in quick response to new regulations.

### Verify Azure Configurations

[Setup Azure credentials](https://www.inspec.io/docs/reference/platforms/) << either in ~/.azure/credentials or by setting the correct environment variables.

``` bash
cd inspec
bundle
bundle exec inspec exec azure-profile -t azure://
```

Adjust tests in `inspec/azure-profile/controls/azure.rb` as needed.

### Verify Host Configurations

``` bash
bundle exec inspec exec ad-profile -t winrm://$ADMIN_USERNAME@$VM_IP_ADDRESS:5986 --password $ADMIN_PASSWORD --ssl --self-signed
```

These tests should mostly fail until the Ansible playbook is run.

### Test Windows Baseline

``` Bash
bundle exec inspec exec https://github.com/dev-sec/windows-baseline/archive/1.1.0.tar.gz -t winrm://$ADMIN_USERNAME@$VM_IP_ADDRESS:5986 --password $ADMIN_PASSWORD --ssl --self-signed
```

## Ansible

### Prep

Setup of WinRM and the upgrade of Powershell to 5.1 should happen during provisioning with Terraform

Install Ansible:
``` bash
pip install ansible
pip install pywinrm[credssp]
```

### Run Ansible

`cd ansible/windows`

1. Edit the `hosts` file with the IP of your test host.
2. Copy `group_vars/windows.yml.example` to `group_vars/windows.yml` and edit with the admin credentials.
3. Encrypt the `windows.yml` credentials file: `ansible-vault encrypt group_vars/windows.yml` and remember your vault password.
3. Run `ansible-playbook` and prompt to ask for the vault password.

``` bash
ansible-playbook playbook.yml -i hosts --ask-vault-pass
```
