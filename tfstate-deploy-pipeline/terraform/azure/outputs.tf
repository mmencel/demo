output "azurerm_storage_account.tfstate.primary_blob_connection_string" {
  value = "${azurerm_storage_account.tfstate.primary_blob_connection_string}"
}

output "azurerm_storage_account.tfstate.primary_access_key" {
  value = "${azurerm_storage_account.tfstate.primary_access_key}"
}

output "azurerm_storage_account.tfstate.primary_blob_endpoint" {
  value = "${azurerm_storage_account.tfstate.primary_blob_endpoint}"
}
