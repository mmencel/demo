provider "azurerm" {
  subscription_id = "${var.subscription_id}"
  client_id       = "${var.client_id}"
  client_secret   = "${var.client_secret}"
  tenant_id       = "${var.tenant_id}"
}

resource "azurerm_resource_group" "tfstate" {
  name     = "tfstate-${var.short_name}"
  location = "${var.location}"
}

resource "azurerm_storage_account" "tfstate" {
  name                     = "tfstate${var.short_name}"
  resource_group_name      = "${azurerm_resource_group.tfstate.name}"
  location                 = "${var.location}"
  account_tier             = "Standard"
  account_replication_type = "GRS"
}

resource "azurerm_storage_container" "tfstate" {
  name                  = "terraform-state"
  resource_group_name   = "${azurerm_resource_group.tfstate.name}"
  storage_account_name  = "${azurerm_storage_account.tfstate.name}"
  container_access_type = "private"
}
