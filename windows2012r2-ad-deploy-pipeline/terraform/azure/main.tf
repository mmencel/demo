provider "azurerm" {
  subscription_id = "${var.subscription_id}"
  client_id       = "${var.client_id}"
  client_secret   = "${var.client_secret}"
  tenant_id       = "${var.tenant_id}"
}

locals = {
  identifier           = "${terraform.workspace}"
  identifer_safe       = "${replace(terraform.workspace,"-","")}"
  virtual_machine_name = "${var.prefix}-dc"
  virtual_machine_fqdn = "${local.virtual_machine_name}.${var.active_directory_domain}"
  custom_data_params   = "Param($RemoteHostName = \"${local.virtual_machine_fqdn}\", $ComputerName = \"${local.virtual_machine_name}\")"
  custom_data_content  = "${local.custom_data_params} ${file("${path.module}/files/winrm.ps1")}"
}

resource "azurerm_resource_group" "dc" {
  name     = "${local.identifier}-dc"
  location = "${var.location}"
}

resource "azurerm_virtual_network" "dc-vnet" {
  name                = "dc-vnet"
  address_space       = ["10.10.0.0/16"]
  location            = "${azurerm_resource_group.dc.location}"
  resource_group_name = "${azurerm_resource_group.dc.name}"
}

resource "azurerm_subnet" "dc-subnet" {
  name                 = "dc-subnet"
  resource_group_name  = "${azurerm_resource_group.dc.name}"
  virtual_network_name = "${azurerm_virtual_network.dc-vnet.name}"
  address_prefix       = "10.10.10.0/24"
}

resource "azurerm_public_ip" "publicip" {
  name                         = "PublicIP"
  location                     = "${var.location}"
  resource_group_name          = "${azurerm_resource_group.dc.name}"
  public_ip_address_allocation = "dynamic"

  tags {
    environment = "test"
  }
}

resource "azurerm_network_security_group" "demo" {
  name                = "demonsg"
  resource_group_name = "${azurerm_resource_group.dc.name}"
  location            = "${var.location}"

  security_rule {
    name                       = "RDP"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "3389"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "WinRM"
    priority                   = 1002
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "5986"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags {
    environment = "test"
  }
}

resource "azurerm_network_interface" "nic" {
  name                      = "${var.prefix}-dc-primary"
  location                  = "${azurerm_resource_group.dc.location}"
  resource_group_name       = "${azurerm_resource_group.dc.name}"
  network_security_group_id = "${azurerm_network_security_group.demo.id}"
  internal_dns_name_label   = "${local.virtual_machine_name}"

  ip_configuration {
    name                          = "primary"
    subnet_id                     = "${azurerm_subnet.dc-subnet.id}"
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = "${azurerm_public_ip.publicip.id}"
  }
}

resource "azurerm_virtual_machine" "domain-controller" {
  name                  = "${local.virtual_machine_name}"
  location              = "${azurerm_resource_group.dc.location}"
  resource_group_name   = "${azurerm_resource_group.dc.name}"
  network_interface_ids = ["${azurerm_network_interface.nic.id}"]
  vm_size               = "Standard_D2_v3"

  delete_os_disk_on_termination    = true
  delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2012-R2-Datacenter"
    version   = "latest"
  }

  storage_os_disk {
    name              = "${local.virtual_machine_name}-disk1"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  storage_data_disk {
    name              = "${local.virtual_machine_name}-datadisk"
    managed_disk_type = "Standard_LRS"
    create_option     = "Empty"
    disk_size_gb      = "100"
    lun               = 0
    caching           = "None"
  }

  os_profile {
    computer_name  = "${local.virtual_machine_name}"
    admin_username = "${var.admin_username}"
    admin_password = "${var.admin_password}"
    custom_data    = "${local.custom_data_content}"
  }

  os_profile_windows_config {
    provision_vm_agent        = true
    enable_automatic_upgrades = true

    # additional_unattend_config {
    #   pass         = "oobeSystem"
    #   component    = "Microsoft-Windows-Shell-Setup"
    #   setting_name = "AutoLogon"
    #   content      = "<AutoLogon><Password><Value>${var.admin_password}</Value></Password><Enabled>true</Enabled><LogonCount>1</LogonCount><Username>${var.admin_username}</Username></AutoLogon>"
    # }

    # # Unattend config is to enable basic auth in WinRM, required for the provisioner stage.
    # additional_unattend_config {
    #   pass         = "oobeSystem"
    #   component    = "Microsoft-Windows-Shell-Setup"
    #   setting_name = "FirstLogonCommands"
    #   content      = "${file("${path.module}/files/FirstLogonCommands.xml")}"
    # }
  }

  tags {
    environment = "test"
  }
}
