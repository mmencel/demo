// the `exit_code_hack` is to keep the VM Extension resource happy
locals {
  enable_winrm       = "(New-Object -TypeName System.Net.WebClient).DownloadFile('https://raw.githubusercontent.com/ansible/ansible/devel/examples/scripts/ConfigureRemotingForAnsible.ps1', 'C:\\ConfigureRemotingForAnsible.ps1'); powershell.exe -ExecutionPolicy ByPass -File C:\\ConfigureRemotingForAnsible.ps1 -enablecredssp -disablebasicauth -verbose"
  upgrade_powershell = "(New-Object -TypeName System.Net.WebClient).DownloadFile('https://raw.githubusercontent.com/jborean93/ansible-windows/master/scripts/Upgrade-PowerShell.ps1', 'C:\\Upgrade-PowerShell.ps1'); Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Force; &C:\\Upgrade-PowerShell.ps1 -Version 5.1 -Verbose"
  exit_code_hack     = "exit 0"
  powershell_command = "${local.enable_winrm}; ${local.upgrade_powershell}; ${local.exit_code_hack}"
}

resource "azurerm_virtual_machine_extension" "node_prep" {
  name                 = "node_prep"
  location             = "${azurerm_virtual_machine.domain-controller.location}"
  resource_group_name  = "${local.identifier}-dc"
  virtual_machine_name = "${azurerm_virtual_machine.domain-controller.name}"
  publisher            = "Microsoft.Compute"
  type                 = "CustomScriptExtension"
  type_handler_version = "1.9"

  settings = <<SETTINGS
{
    "commandToExecute": "powershell.exe -Command \"(New-Object -TypeName System.Net.WebClient).DownloadFile('https://raw.githubusercontent.com/ansible/ansible/devel/examples/scripts/ConfigureRemotingForAnsible.ps1', 'C:\\ConfigureRemotingForAnsible.ps1'); powershell.exe -ExecutionPolicy ByPass -File C:\\ConfigureRemotingForAnsible.ps1 -enablecredssp -disablebasicauth -verbose; (New-Object -TypeName System.Net.WebClient).DownloadFile('https://raw.githubusercontent.com/jborean93/ansible-windows/master/scripts/Upgrade-PowerShell.ps1', 'C:\\Upgrade-PowerShell.ps1'); Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Force; &C:\\Upgrade-PowerShell.ps1 -Version 5.1 -username ${var.admin_username} -password ${var.admin_password} -Verbose; shutdown -r -t 10; exit 0\""
}
SETTINGS
}

# // NOTE: we **highly recommend** not using this configuration for your Production Environment
# // this provisions a single node configuration with no redundancy.
# resource "azurerm_virtual_machine_extension" "create-active-directory-forest" {
#   name                 = "create-active-directory-forest"
#   location             = "${azurerm_virtual_machine.domain-controller.location}"
#   resource_group_name  = "${local.identifier}-dc"
#   virtual_machine_name = "${azurerm_virtual_machine.domain-controller.name}"
#   publisher            = "Microsoft.Compute"
#   type                 = "CustomScriptExtension"
#   type_handler_version = "1.9"


#   settings = <<SETTINGS
#     {
#         "commandToExecute": "powershell.exe -Command \"${local.powershell_command}\""
#     }
# SETTINGS
# }

