###
# OUTPUTS
###

# When using a Dynamic IP, must pull it from "data". 
# It's not provisioned in time for output to pull directly from the resource.
data "azurerm_public_ip" "dcip" {
  name                = "${azurerm_public_ip.publicip.name}"
  resource_group_name = "${azurerm_resource_group.dc.name}"
}

output "azure_instance_public_ip" {
  value = "${data.azurerm_public_ip.dcip.ip_address}"
}
