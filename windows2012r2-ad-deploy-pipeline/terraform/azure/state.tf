terraform {
  backend "azurerm" {
    resource_group_name  = "tfstate-pre"
    storage_account_name = "tfstatepre"
    container_name       = "terraform-state"
    key                  = "core.terraform.tfstate"
  }
}
