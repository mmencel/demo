terraform {
  backend "azurerm" {
    resource_group_name  = "tfstate-${var.prefix}"
    storage_account_name = "tfstate${var.prefix}"
    container_name       = "terraform-state"
    key                  = "core.terraform.tfstate"
  }
}
