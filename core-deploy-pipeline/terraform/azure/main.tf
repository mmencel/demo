provider "azurerm" {
  subscription_id = "${var.subscription_id}"
  client_id       = "${var.client_id}"
  client_secret   = "${var.client_secret}"
  tenant_id       = "${var.tenant_id}"
}

locals = {
  identifier     = "${terraform.workspace}"
  identifer_safe = "${replace(terraform.workspace,"-","")}"
}

resource "azurerm_resource_group" "core" {
  name     = "${local.identifier}"
  location = "${var.location}"
}

resource "azurerm_resource_group" "images" {
  name     = "${local.identifier}-images"
  location = "${var.location}"
}

resource "azurerm_storage_account" "core" {
  name                     = "${local.identifer_safe}"
  resource_group_name      = "${azurerm_resource_group.core.name}"
  location                 = "${var.location}"
  account_tier             = "Standard"
  account_replication_type = "LRS"
}
